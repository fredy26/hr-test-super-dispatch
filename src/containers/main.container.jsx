import { connect } from 'react-redux'
import { withRouter } from 'react-router'

import { bindActionCreators } from 'redux'
import {
  NAME, orderedPopularMoviesIDsSelector,
  searchResultsTitlesSelector, isSearchingSelector
} from '../store/reducer'
import { actions } from '../store/actions'

import MainView from '../views/main'

const mapStateToProps = (state) => {
  return {
    ids: orderedPopularMoviesIDsSelector(state[NAME]),
    searchResults: searchResultsTitlesSelector(state[NAME]),
    isSearching: isSearchingSelector(state[NAME])
  }
}

const mapDispatchToProps = (dispatch) => ({
  ...bindActionCreators({
    ...actions
  }, dispatch)
})

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MainView))
