import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { NAME, movieByIDSelector } from '../store/reducer'
import { actions } from '../store/actions'

import MovieCard from '../components/MovieCard'

const mapStateToProps = (state, props) => {
  const id = props.id
  return {
    movie: movieByIDSelector(state[NAME], id)
  }
}

const mapDispatchToProps = (dispatch) => ({
  ...bindActionCreators({
    ...actions
  }, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(MovieCard)
