import { connect } from 'react-redux'
import { NAME, genreListSelector } from '../store/reducer'

import MovieGenres from '../components/MovieGenres'

const mapStateToProps = (state, props) => {
  return {
    genres: genreListSelector(state[NAME])
    // genres: selectGenresAsStringSelector(state[NAME], props)
  }
}

export default connect(mapStateToProps)(MovieGenres)
