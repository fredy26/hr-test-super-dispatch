import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { NAME, movieDetailsSelector, movieDetailsGenresSelector, recommendedMoviesSelector } from '../store/reducer'
import { actions } from '../store/actions'

import DetailsView from '../views/details'

const mapStateToProps = (state, props) => {
  return {
    details: movieDetailsSelector(state[NAME]),
    genres: movieDetailsGenresSelector(state[NAME]),
    recommended: recommendedMoviesSelector(state[NAME])
  }
}

const mapDispatchToProps = (dispatch) => ({
  ...bindActionCreators({
    ...actions
  }, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(DetailsView)
