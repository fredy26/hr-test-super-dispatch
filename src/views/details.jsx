import React, { Component } from 'react'
import PropTypes from 'prop-types'
import DetailsLayout from '../components/DetailsLayout'
import MovieInfo from '../components/MovieInfo'
import Recommended from '../components/Recommended'

class DetailsView extends Component {
  componentDidMount () {
    const { id } = this.props.match.params
    this.props.getDetails(id)
    this.props.getRecommended(id)
  }

  render () {
    const { details, genres, recommended, location } = this.props
    return (
      <DetailsLayout image={details.backdrop_path} location={location}>
        <MovieInfo details={details} genres={genres} />
        <Recommended recommended={recommended} />
      </DetailsLayout>
    )
  }
}

DetailsView.propTypes = {
  details: PropTypes.object,
  recommended: PropTypes.array,
  genres: PropTypes.string, // TODO: Get list of genres from store
  getDetails: PropTypes.func.isRequired, // action to get a single movie data
  getRecommended: PropTypes.func.isRequired // action to get recommended movies list
}

export default DetailsView
