import React, { Component } from 'react'
import PropTypes from 'prop-types'
import _throttle from 'lodash/throttle'

import MovieGrid from '../components/MovieGrid'
import MovieCardContainer from '../containers/MovieCard.container'
import SingleSelect from '@atlaskit/single-select'

class MainView extends Component {
  constructor (props) {
    super(props)
    this.loadMore = this.loadMore.bind(this)
    this.handleSelectedSearchResult = this.handleSelectedSearchResult.bind(this)
    this.handleSearch = _throttle(this.handleSearch, 700).bind(this)
  }

  handleSearch (query) {
    this.props.search(query)
  }

  loadMore (page) {
    this.props.getPopular(page)
  }

  handleSelectedSearchResult ({ item: { value: id } }) {
    console.log('selected id :', id)
    this.props.history.push(`/${id}`)
  }

  componentDidMount () {
    // FIXME: (cheap eval) check if any data already is in store. If it is - skip APi call, use redux as a cash.
    // TODO: (smart eval) we need a time stamp and periodic refreshes of the whole tree if the stamp is too old
    // TODO: OR/AND !Or make a notification if any old data found with a button and recommendation to refresh... refreshing all of the tree
    // NOTE: We CANNOT update the results per page as order may change during that time and model (order items actually) may double...
    // NOTE: overall Ux may be hurt because of the infinite scroll solution and heavy cards
    this.props.getPopularIfNoCash()
    this.props.getGenres()
  }

  render () {
    return (
      <div>
        <MovieGrid onLoadMore={this.loadMore}>
          <SingleSelect
            shouldFitContainer
            items={this.props.searchResults}
            onFilterChange={this.handleSearch}
            onSelected={this.handleSelectedSearchResult}
            placeholder='Название фильма...'
            noMatchesFound='Ничего не найдено'
            hasAutocomplete
            isLoading={this.props.isSearching}
            loadingMessage='LOOKING FOR KINOSHECHKA...'
          />
          {this.props.ids.map(id => <MovieCardContainer key={id} id={id} />)}
        </MovieGrid>
      </div>
    )
  }
}
MainView.propTypes = {
  getPopular: PropTypes.func.isRequired, // action to get a page with 20 (fixed by API) popularMovies
  getPopularIfNoCash: PropTypes.func.isRequired,
  search: PropTypes.func.isRequired,
  searchResults: PropTypes.array,
  isSearching: PropTypes.bool,
  ids: PropTypes.array.isRequired // movies ids
}

export default MainView
