import _isEmpty from 'lodash/isEmpty'

// TODO: better move all of the logic to reducers
export default function makeStringFromGenres (ids, genres) {
  if (_isEmpty(genres)) return 'loading...'
  if (!ids || ids.length === 0) return 'ВНЕ ЖАНРОВЫХ РАМОК'
  const genresArray = ids.map(id => genres[id] ? genres[id].name : null).filter(genre => genre)
  return genresArray.length > 0 ? genresArray.join(', ') : 'loading...'
}
