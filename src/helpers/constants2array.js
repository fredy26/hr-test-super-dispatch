export default function extractConstantsToArray (actionSet) {
  const { ACTION, SUCCESS, ERROR } = actionSet
  return [ACTION, SUCCESS, ERROR]
}
