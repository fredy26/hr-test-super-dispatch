export default function numbersWithSpaces (number) {
  return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ')
}
