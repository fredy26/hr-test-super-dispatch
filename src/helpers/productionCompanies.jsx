export default function productionCompanies (companies = []) {
  const names = companies.map(({ name }) => name)
  return names.length > 0 ? names.join(', ') : ''
}
