import { reducer as movieReducer, NAME as movieReducerName } from './reducer'

export default {
  [movieReducerName]: movieReducer
}
