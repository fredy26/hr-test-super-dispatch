/* eslint-disable camelcase */
import { createSelector } from 'reselect'
import _isEqual from 'lodash/isEqual'
import _isEmpty from 'lodash/isEmpty'
import { handleActions, combineActions } from 'redux-actions'
import * as actionTypes from './action.types'
import normalize from '../helpers/normalizr'

export const NAME = 'MOVIES'

const initialState = {
  isLoading: false,
  loaders: { // TODO: make independent loaders in favor of separate loaders pattern
    recommended: false,
    details: false,
    searching: false
  },
  genresN: {},
  sortOrder: [],
  popularN: {},
  details: {},
  recommended: [],
  searchResults: [],
  page: 0,
  total_results: 0,
  total_pages: 0
}

export const reducer = handleActions({
  [actionTypes.CLEAR_POPULAR.ACTION]: (state) => ({
    ...state,
    popularN: {},
    sortOrder: []
  }),
  [actionTypes.GET_POPULAR_MOVIES.SUCCESS]: (state, action) => {
    const { payload: { results: popular, page, total_results, total_pages } } = action
    const { normalized: popularN, sortOrder } = normalize(popular)
    const tstate = _isEqual(popular, state.popular)
      ? state
      : ({ ...state,
        popularN: { ...state.popularN, ...popularN },
        sortOrder: state.sortOrder.concat(sortOrder),
        page,
        total_results,
        total_pages
      })
    return tstate
  },

  [actionTypes.SEARCH_MOVIES.ACTION]: (state) => {
    return ({ ...state, loaders: { ...state.loaders, searching: true } })
  },
  [actionTypes.SEARCH_MOVIES.ERROR]: (state) => {
    return ({ ...state, loaders: { ...state.loaders, searching: false } })
  },
  [actionTypes.SEARCH_MOVIES.SUCCESS]: (state, action) => {
    const { payload: { results: searchResults } } = action
    return ({ ...state, searchResults, loaders: { ...state.loaders, searching: false } })
  },

  [actionTypes.GET_MOVIE_DETAILS.ACTION]: (state, action) => {
    const details = {}
    return ({ ...state, details })
  },
  [actionTypes.GET_MOVIE_DETAILS.SUCCESS]: (state, action) => {
    const { payload: details } = action
    return ({ ...state, details })
  },

  [actionTypes.GET_GENRES.SUCCESS]: (state, action) => {
    const { payload: { genres } } = action
    const genresN = normalize(genres).normalized
    return _isEqual(genresN, state.genres)
      ? state
      : ({ ...state, genresN })
  },

  [actionTypes.GET_RECOMMENDED_MOVIES.ACTION]: (state, action) => {
    // console.info('%cGET_RECOMMENDED_MOVIES.ACTION fired', 'background-color:orange;color:white;line-height:1.2rem;')
    const recommended = []
    return ({ ...state, recommended, loaders: { ...state.loaders, recommended: true } })
  },
  [actionTypes.GET_RECOMMENDED_MOVIES.SUCCESS]: (state, action) => {
    const { payload: { results: recommended } } = action
    return ({ ...state, recommended, loaders: { ...state.loaders, recommended: false } })
  },
  [actionTypes.GET_RECOMMENDED_MOVIES.ERROR]: (state, action) => {
    return ({ ...state, loaders: { ...state.loaders, recommended: false } })
  },

  [combineActions(...actionTypes.PENDING_ACTIONS)]: (state) =>
    state.isLoading ? state : ({ ...state, isLoading: true }),
  [combineActions(...actionTypes.FINITE_ACTIONS)]: (state) =>
    state.isLoading ? ({...state, isLoading: false}) : state
}, initialState)

// SELECTORS FOR STORE DATA
export const popularMoviesSelector = state => state.popularN
export const genresListSelector = state => state.genresN
export const currentPageSelector = state => state.page
export const orderedPopularMoviesIDsSelector = state => state.sortOrder
export const isSearchingSelector = state => state.loaders.searching
export const searchResultsSelector = state => state.searchResults
export const searchResultsTitlesSelector = createSelector(
  searchResultsSelector,
  results => [{
    heading: 'КИНОШЕЧКИ',
    items: results.map(({ title, id }) => ({ title, id })).map(({ title, id }) => ({ content: title, value: id }))
  }]
)
export const popularMoviesIDsSelector = createSelector(
  popularMoviesSelector,
  movies => Object.keys(movies)
)
export const movieByIDSelector = (state, id) => state.popularN[id]
export const movieDetailsSelector = (state) => state.details
export const movieDetailsGenresSelector = (state) => _isEmpty(state.details) ? '' : state.details.genres.map(({name}) => name).join(', ')
export const recommendedMoviesSelector = (state) => state.recommended

export const movieGenresIDsSelector = (state, props) => _isEmpty(state.popularN) ? null : state.popularN[props.id].genre_ids
export const genreListSelector = (state) => state.genresN
// TODO: test this one to replace logic built-in with Genres component
export const selectGenresAsStringSelector = createSelector(
  movieGenresIDsSelector,
  genreListSelector,
  (genreIDs, genreList) => (genreIDs && genreList) ? genreIDs.map(id => genreList[id] ? genreList[id].name : '').join(', ') : 'loading...'
)
export const isFinalPageSelector = (state) => (state.page === state.total_pages)

/* eslint-enable camelcase */
