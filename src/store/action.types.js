import { defineAction } from 'redux-define'
const movieDB = defineAction('movieDB')

export const GET_POPULAR_MOVIES = movieDB.defineAction('GET_POPULAR_MOVIES', ['ERROR', 'SUCCESS'])
export const GET_GENRES = movieDB.defineAction('GET_GENRES', ['ERROR', 'SUCCESS'])
export const SEARCH_MOVIES = movieDB.defineAction('SEARCH_MOVIES', ['ERROR', 'SUCCESS'])
export const GET_MOVIE_DETAILS = movieDB.defineAction('GET_MOVIE_DETAILS', ['ERROR', 'SUCCESS'])
export const GET_RECOMMENDED_MOVIES = movieDB.defineAction('GET_RECOMMENDED_MOVIES', ['ERROR', 'SUCCESS'])
export const CLEAR_POPULAR = movieDB.defineAction('CLEAR_POPULAR')

// TODO: we should generate these classes with code
// TODO: separate basic common action constants into pending and finite

// actions that initiates asynchronous something
export const PENDING_ACTIONS = [
  GET_POPULAR_MOVIES.ACTION,
  GET_GENRES.ACTION,
  // SEARCH_MOVIES.ACTION,
  GET_MOVIE_DETAILS.ACTION,
  GET_RECOMMENDED_MOVIES.ACTION
]
// actions that either resolves to error or data
export const FINITE_ACTIONS = [
  GET_POPULAR_MOVIES.SUCCESS,
  GET_POPULAR_MOVIES.ERROR,
  GET_GENRES.SUCCESS,
  GET_GENRES.ERROR,
  // SEARCH_MOVIES.SUCCESS,
  // SEARCH_MOVIES.ERROR,
  GET_MOVIE_DETAILS.SUCCESS,
  GET_MOVIE_DETAILS.ERROR,
  GET_RECOMMENDED_MOVIES.SUCCESS,
  GET_RECOMMENDED_MOVIES.ERROR
]
