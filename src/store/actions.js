/* eslint-disable camelcase */
import { RSAA } from 'redux-api-middleware'
import extractConstantsToArray from '../helpers/constants2array'
import * as actionTypes from './action.types'

import { NAME, popularMoviesIDsSelector } from './reducer'
// TODO: Move all settings to dedicated file
const entryPoint = 'https://api.themoviedb.org/3'
const api_key = '9a995d3359ed21036284913878476fbc'
export const language = window.navigator.userLanguage || window.navigator.language || 'ru-RU'
const PAGE_SIZE = 20 // defined as a constant by MovieDB API
// TODO: add api_key and language to every request path via middleware

export const actions = {
  clearPopular: () => ({ type: actionTypes.CLEAR_POPULAR }),

  search: (query, page = 1) => ({
    [RSAA]: {
      endpoint: `${entryPoint}/search/movie?api_key=${api_key}&query=${query}&include_adult=true&language=${language}&page=${page}`,
      method: 'GET',
      types: extractConstantsToArray(actionTypes.SEARCH_MOVIES),
      headers: { 'Content-Type': 'application/json' }
    }
  }),
  // TODO: Apply and test store2localstorage solution middleware
  getPopularIfNoCash: () => (dispatch, getState) => { // NOTE: using store as a session cash
    const totalMoviesInCash = popularMoviesIDsSelector(getState()[NAME]).length
    if (totalMoviesInCash < PAGE_SIZE) {
      // HACK: looks like we got an empty or corrupted or populars reduced to 10 movies(really?) data
      dispatch(actions.clearPopular())
      return dispatch(actions.getPopular())
    }
  },

  getPopular: (page = 1) => ({
    [RSAA]: {
      endpoint: `${entryPoint}/movie/popular?api_key=${api_key}&language=${language}&page=${page}`,
      method: 'GET',
      types: extractConstantsToArray(actionTypes.GET_POPULAR_MOVIES),
      headers: { 'Content-Type': 'application/json' }
    }
  }),

  getGenres: () => ({
    [RSAA]: {
      endpoint: `${entryPoint}/genre/movie/list?api_key=${api_key}&language=${language}`,
      method: 'GET',
      types: extractConstantsToArray(actionTypes.GET_GENRES),
      headers: { 'Content-Type': 'application/json' }
    }
  }),

  getDetails: (id) => ({
    [RSAA]: {
      endpoint: `${entryPoint}/movie/${id}?api_key=${api_key}&language=${language}`,
      method: 'GET',
      types: extractConstantsToArray(actionTypes.GET_MOVIE_DETAILS),
      headers: { 'Content-Type': 'application/json' }
    }
  }),

  getRecommended: (id, page = 1) => ({
    [RSAA]: {
      endpoint: `${entryPoint}/movie/${id}/recommendations?api_key=${api_key}&language=${language}&page=${page}`,
      method: 'GET',
      types: extractConstantsToArray(actionTypes.GET_RECOMMENDED_MOVIES),
      headers: { 'Content-Type': 'application/json' }
    }
  })
}

/* eslint-enable camelcase */
