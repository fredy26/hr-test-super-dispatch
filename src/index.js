import { render } from 'react-dom'
import '@atlaskit/css-reset'
import './assets/index.css'
import routes from './routes'

render(routes, document.getElementById('root'))
