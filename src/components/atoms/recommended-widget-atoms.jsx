import styled from 'styled-components'

export const WidgetHeader = styled.h1`
  font-size: 2.07142857em;
  font-style: inherit;
  font-weight: 600;
  letter-spacing: -0.01em;
  line-height: 1.10344828;
  margin-top: 0px;
`

export const WidgetWrapper = styled.div`
  position: relative;
  left: -5px;
  display: flex;
  flex-direction: row;
  align-content: space-between;
  overflow-y: scroll;
  width: 100%;
  height: 280px;
  padding: 10px 5px 10px 10px;
`

export const MovieMiniCard = styled.div`
  border-radius: 5px;
  background-color: #ffffff;
  width: 342px;
  transition: all 0.3s ease;
  flex: 0 0 auto;
  margin-top: 5px;
  margin-left: 0px;
  margin-right: 2%;
  padding: 10px;
  color: #323538;
  box-shadow: 0px 1px 3px 0px rgba(255, 255, 255, 0.5);
  img {
    max-width: 100%;
    border-bottom: 1px solid #e2e2e2;
    margin-bottom: 5px;
  }
  &:hover {
    transform: scale(1.05);
    box-shadow: 0px 1px 10px 2px rgba(255, 255, 255, 0.5);
    transition: all 0.4s;
  }
`

export const MiniCardTitle = styled.span`
  display: contents;
  line-height: 2.6rem;
  margin-left: 5px;
  font-size: 1.2rem;
`
