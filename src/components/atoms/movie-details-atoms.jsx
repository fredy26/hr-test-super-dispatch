import styled from 'styled-components'

export const Background = styled.div`
  width: 100%;
  height: 100%;
  background: linear-gradient(to right,
  rgba(220, 225, 250, 0.8),
  rgba(255, 255, 255, 0.9)),
  url("${props => props.image ? ('http://image.tmdb.org/t/p/w780/' + props.image) : ''}");
  background-size: cover;
  background-position: center;
`

export const PageWrapper = styled.div`
  border-top: 2px dotted #e3e3e3;
  position: relative;
  display: block;
  width: 100%;
  height: 1060px;
  overflow: hidden;
  transition: all 0.4s;
`

export const ContentWrapper = styled.div`
  position: relative;
  width: 90%;
  margin: 30px auto;
  height: 90%;
  z-index: 2;
  border-radius: 10px;
`

export const BigPoster = styled.img.attrs({
  src: props => props.poster ? 'http://image.tmdb.org/t/p/w342/' + props.poster : ''
})`
  display: inline-block;
  float: left;
  margin-right: 20px;
  height: 420px;
  transition: all 0.4s;
  border-radius: 5px;
  box-shadow: 5px 5px 20px -5px rgba(0, 0, 0, 0.9);
`

export const MovieHeader = styled.div`
  display: inline-block;
  width: 60%;
  float: left;
  padding: 20px;
  overflow: hidden;
  h1 {
    display: inline-block;
    color: black;
    font-weight: 400;
    font-size: 48px;
  }
  h4 {
    color: #555;
    font-weight: 400;
  }
`

export const Minutes = styled.div`
  display: inline-block;
  margin-top: 15px;
  color: #555;
  padding: 5px;
  border-radius: 5px;
  border: 1px solid rgba(0, 0, 0, 0.05);
`
export const GenresWrapper = styled.p`
  display: inline-block;
  margin-top: 15px;
  color: black;
  margin-left: 10px;
  padding: 5px;
  background-color: rgba(255, 255, 255, 0.5);
  border-radius: 5px;
`
export const MovieDescription = styled.p`
  color: #545454;
  width: 100%;
  margin-top: 15px;
  line-height: 1.6rem;
  text-overflow: ellipsis;
  word-break: normal;
`

export const TextInfoWrapper = styled.div`
  margin-top: 15px;
  display: inline-block;
  padding: 0;
  width: 100%;
  h1 {
    font-weight: 400;
  }
`

export const TextInfoColumn = styled.div`
  display: inline-block;
  width: 35%;
  margin-right: 20px;
  background-color: rgba(245, 245, 245, 0.1);
  box-shadow: 0px 0px 10px 5px rgba(245, 245, 245, 0.05);
`

export const TextInfoRow = styled.div`
  margin: 10px 0px;
`

export const EasyHeader = styled.h5`
  font-weight: 300;
  font-size: 1.2rem;
`
export const SmallHeader = styled.h4`
  display: inline-block;
  word-spacing: 5%;
  margin-right: 5px;
`

export const ShortLink = styled.a`
  display: inline-block;
  width: 200px;
  overflow: hidden;
  text-overflow: ellipsis;
  font-weigh: 300;
  color: blue;
`
