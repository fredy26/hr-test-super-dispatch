import styled from 'styled-components'

export const CardWrapper = styled.div`
  position: relative;
  display: block;
  width: 800px;
  height: 350px;
  margin: 80px auto; 
  overflow: hidden;
  border-radius: 10px;
  transition: all 0.4s;
  box-shadow: 0px 0px 120px -25px rgba(0,0,0, 0.5);
  &:hover{
    transform: scale(1.02);
    box-shadow: 0px 0px 80px -25px rgba(0,0,0, 0.5);
    transition: all 0.4s;
  }
`
export const InfoSection = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
  background-blend-mode: multiply;
  z-index: 2;
  border-radius: 10px;
  background: linear-gradient(to right, #e5e6e6 50%, transparent 100%);
  display: block;
`

export const MovieHeader = styled.div`
  position: relative;
  padding: 25px;
  height: 40%;
  width: 60%;
  h1{
    margin-top: 0px;
    color: black;
    font-weight: 400;
  }
  h4{
    margin-top: 5px;
    color: #555;
    font-weight: 500;
    font-size: 1.6rem;
  }
`

export const CardBottom = styled.div`
  position: relative;
`

export const Rating = styled.span`
  color: lemonchiffon;
  padding: 5px;
  border-radius: 5px;
  border: 1px solid rgba(0, 0, 0, 0.05);
  font-size: 1.6rem;
  background-color: rgba(40, 200, 100, 0.9);
  position: relative;
  float: left;
  top: -130px;
  left: 76%;
  width: 50px;
  text-align: center;
`

export const PosterWrapper = styled.img.attrs({
  src: props => props.src
})`
  position: relative;
  float: left;
  margin-right: 20px;
  height: 120px;
  box-shadow: 0px 0px 20px 1px rgba(0,0,0,0.3)
  
`
export const GenresWrapper = styled.p`
  padding: 5px;
  border-radius: 5px;
  border: 1px solid rgba(0, 0, 0, 0.05);
  color: #ffffff;
  background-color: rgba(0, 0, 0, 0.6);
  margin-left: 10px;
  position: relative;
  float: right;
  top: 170px;
`

export const MovieDescriptonWrapper = styled.div`
  padding: 25px;
  width: 50%;
  color: #545454;
  overflow: hidden;
  text-overflow: ellipsis;
  display: block;
  position: absolute;
  white-space: normal;
  p {
    height:135px;
    white-space: normal;
  }
`

export const Backdrop = styled.div`
  background: url("${props => props.src}");
  width: 80%;
  background-position: -100% 10% !important;
  position: absolute;
  top: 0;
  z-index: 0;
  height: 100%; right: 0;
  background-size: cover;
  border-radius: 11px;
`
