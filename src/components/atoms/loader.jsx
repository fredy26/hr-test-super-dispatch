import React from 'react'
import { ScaleLoader } from 'react-spinners'

const Loader = () => <ScaleLoader color='#28c864e6' className='loader' />

export default Loader
