import React, { Component } from 'react'
import PropTypes from 'prop-types'
import isEqual from 'lodash/isEqual'
import { Link } from 'react-router-dom'
import {
  CardWrapper, InfoSection, MovieHeader, PosterWrapper, Rating,
  MovieDescriptonWrapper, Backdrop, CardBottom, GenresWrapper
} from './atoms/movie-card-atoms'

import Genres from '../containers/MovieGenres.container'

class MovieCard extends Component {
  constructor (props) {
    super(props)
    this.overviewPlaceholder = 'Версия на вашем языке отсутствует. Добавив её самостоятельно, Вы поможете проекту.'
  }

  shouldComponentUpdate (nextProps) {
    return !isEqual(this.props.movie, nextProps.movie)
  }
  // TODO: Get rid of the anchor link underline
  render () {
    const { movie, id } = this.props
    const releaseDate = new Date(movie.release_date).getFullYear()
    const voteAverage = movie.vote_average.toString().length === 1 ? movie.vote_average + '.0' : movie.vote_average
    return (
      <Link to={`/${id}`}>
        <CardWrapper>
          <InfoSection>
            <MovieHeader>
              <PosterWrapper src={`http://image.tmdb.org/t/p/w185/${movie.poster_path}`} />
              <h1>{movie.title}</h1>
              <h4>{releaseDate}</h4>
            </MovieHeader>
            <MovieDescriptonWrapper>
              <p>{movie.overview || this.overviewPlaceholder}</p>
            </MovieDescriptonWrapper>
            <CardBottom>
              <Rating>{+voteAverage === 0 ? 'n/a' : voteAverage}</Rating>
              <Genres ids={movie.genre_ids} render={
                (genres) =>
                  <GenresWrapper>{genres}</GenresWrapper>
              } />
            </CardBottom>
          </InfoSection>
          <Backdrop src={`http://image.tmdb.org/t/p/w500/${movie.backdrop_path || movie.poster_path}`} />
        </CardWrapper>
      </Link>
    )
  }
}

MovieCard.propTypes = {
  movie: PropTypes.object.isRequired, // get a page with 20 (fixed by API) popularMovies
  id: PropTypes.number.isRequired
}
export default MovieCard
