import PropTypes from 'prop-types'
import makeStringFromGenres from '../helpers/makeStringFromGenres'

export const Genres = ({ ids, genres, render }) => {
  return (
    render(makeStringFromGenres(ids, genres))
  )
}
Genres.propTypes = {
  ids: PropTypes.array,
  genres: PropTypes.object
}
export default Genres
