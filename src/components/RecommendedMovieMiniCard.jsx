import React from 'react'
import PropTypes from 'prop-types'
import { MovieMiniCard, MiniCardTitle } from './atoms/recommended-widget-atoms'

class RecommendedMovieMiniCard extends React.Component {
  constructor (props) {
    super(props)
    this.handleClick = this.handleClick.bind(this)
  }
  handleClick () {
    this.props.onSelect(this.props.id)
  }
  render () {
    const { img, title } = this.props
    return (
      <MovieMiniCard onClick={this.handleClick}>
        <img src={`http://image.tmdb.org/t/p/w342/${img}`} alt={title} />
        <MiniCardTitle>{title}</MiniCardTitle>
      </MovieMiniCard>
    )
  }
}

RecommendedMovieMiniCard.propTypes = {
  img: PropTypes.string,
  title: PropTypes.string
}
export default RecommendedMovieMiniCard
