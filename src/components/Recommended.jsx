import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router'

import { WidgetWrapper, WidgetHeader } from '../components/atoms/recommended-widget-atoms'
import RecommendedMovieMiniCard from '../components/RecommendedMovieMiniCard'

class Recommended extends Component {
  constructor (props) {
    super(props)
    this.goDetails = this.goDetails.bind(this)
  }
  goDetails (id) {
    this.props.history.push(`/${id}`) // FIXME: Having a pure component router blocker along the component tree.
    // TODO: Investigate
  }
  render () {
    const { recommended } = this.props
    return recommended.length > 0
      ? (
        <React.Fragment>
          <WidgetHeader>Вам понравится:</WidgetHeader>
          <WidgetWrapper>
            {recommended.map(
              ({ backdrop_path: backdropPath, title, id }) =>
                <RecommendedMovieMiniCard
                  onSelect={this.goDetails}
                  img={backdropPath}
                  title={title}
                  id={id} key={id} />
            )}
          </WidgetWrapper>
        </React.Fragment>
      )
      : null
  }
}
Recommended.propTypes = {
  recommended: PropTypes.array
}

export default withRouter(Recommended)
