import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { language } from '../store/actions'

import {
  BigPoster, MovieHeader, Minutes, GenresWrapper, MovieDescription, TextInfoWrapper,
  TextInfoColumn, TextInfoRow, EasyHeader
} from '../components/atoms/movie-details-atoms'
import productionCompanies from '../helpers/productionCompanies'
import numbersWithSpaces from '../helpers/numbersWithSpaces'

class MovieInfo extends Component {
  constructor (props) {
    super(props)
    this.overviewPlaceholder = 'Версия на вашем языке отсутствует. Добавив её самостоятельно, Вы поможете проекту.'
  }
  render () {
    const { details, genres } = this.props
    // TODO: move away logic to selectors and reducer for releaseDate, productionCompanies, overviewPlaceholder, etc
    const releaseDate = new Date(details.release_date).toLocaleDateString(language, { year: 'numeric', month: 'long', day: 'numeric' })
    const originalLanguage = details.original_language ? details.original_language.toUpperCase() : ''
    const revenue = numbersWithSpaces(+details.revenue)
    const budget = numbersWithSpaces(+details.budget)
    return (
      <React.Fragment>
        <BigPoster poster={details.poster_path} />
        <MovieHeader>
          <h1>{details.title}</h1>
          <h3>{details.tagline || productionCompanies(details.production_companies) || 'n/a'}</h3>
          <Minutes>{`${details.runtime || 'Сколько-то'} мин`}</Minutes>
          <GenresWrapper>{genres || 'ВНЕ ЖАНРОВЫХ РАМОК'}</GenresWrapper>
          <MovieDescription>{details.overview || this.overviewPlaceholder}</MovieDescription>
        </MovieHeader>
        <TextInfoWrapper>
          <TextInfoColumn>
            <TextInfoRow>
              <EasyHeader>Релиз: </EasyHeader> <span>{releaseDate}</span>
            </TextInfoRow>
            <TextInfoRow>
              <EasyHeader>Бюджет: </EasyHeader> $&nbsp;<span>{budget}</span>
            </TextInfoRow>
            <TextInfoRow>
              <EasyHeader>Оригинальный язык: </EasyHeader> <span>{originalLanguage}</span>
            </TextInfoRow>
            <TextInfoRow>
              <EasyHeader>Оригинальное название: </EasyHeader> <span>{details.original_title}</span>
            </TextInfoRow>
          </TextInfoColumn>
          <TextInfoColumn>
            <TextInfoRow>
              <EasyHeader>Сборы: </EasyHeader> $<span>{revenue}</span>
            </TextInfoRow>
            <TextInfoRow>
              <EasyHeader>Всего проголосовало: </EasyHeader> <span>{details.vote_count}</span>&nbsp;<span>человек</span>
            </TextInfoRow>
            <TextInfoRow>
              <EasyHeader>Средняя оценка: </EasyHeader> <span>{details.vote_average}</span>
            </TextInfoRow>
            <TextInfoRow>
              <EasyHeader> Кол-во запросов в базе MOVIEDB: </EasyHeader> <span>{details.popularity}</span>
            </TextInfoRow>
          </TextInfoColumn>
        </TextInfoWrapper>
      </React.Fragment>
    )
  }
}

MovieInfo.propTypes = {
  details: PropTypes.object,
  genres: PropTypes.string
}

export default MovieInfo
