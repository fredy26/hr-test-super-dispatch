import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Background, PageWrapper, ContentWrapper } from '../components/atoms/movie-details-atoms'
import Loader from '../components/atoms/loader'

class DetailsLayout extends Component {
  render () {
    const children = React.Children.toArray(this.props.children)
    const haveFirstChild = (this.props.children && children[0])
    const haveSecondChild = (this.props.children && children[1])

    return (
      <Background image={this.props.image}>
        <PageWrapper>
          <ContentWrapper>
            {haveFirstChild ? children[0] : <Loader key={0} loading />}
            {haveSecondChild ? children[1] : <Loader key={1} loading />}
          </ContentWrapper>
        </PageWrapper>
      </Background>
    )
  }
}
DetailsLayout.propTypes = {
  image: PropTypes.string
}

export default DetailsLayout
