import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import Page, { Grid, GridColumn } from '@atlaskit/page'
import InfiniteScroll from 'react-infinite-scroller'
import Loader from '../components/atoms/loader'

class MovieGrid extends PureComponent {
  renderCardsInfinite (children) {
    return (this.props.children && children[1])
      ? (
        <InfiniteScroll
          pageStart={1}
          loadMore={this.props.onLoadMore}
          hasMore={true || false}
          loader={<Loader key={new Date()} />}
          useWindow
        >
          { children.filter((child, index) => index > 0) }
        </InfiniteScroll>
      )
      : <Loader loading />
  } // TODO: replace text with loaders and Skeleton loaders

  render () {
    const children = React.Children.toArray(this.props.children)
    return (
      <Page>
        <div style={{ margin: '25px auto' }}>
          <Grid>
            <GridColumn medium={12}>
              {(this.props.children && children[0])}
            </GridColumn>
            <GridColumn>
              {this.renderCardsInfinite(children)}
            </GridColumn>
          </Grid>
        </div>
      </Page>
    )
  }
}

MovieGrid.propTypes = {
  onLoadMore: PropTypes.func
}

export default MovieGrid
