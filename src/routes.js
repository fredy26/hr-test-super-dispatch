import React from 'react'
import { createStore, applyMiddleware, combineReducers } from 'redux'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import { composeWithDevTools } from 'redux-devtools-extension'
import { apiMiddleware } from 'redux-api-middleware'
import thunk from 'redux-thunk'
import reducers from './store'
import MainViewContainer from './containers/Main.container'
import DetailsViewContainer from './containers/Details.container'

const store = createStore(
  combineReducers(reducers),
  composeWithDevTools(applyMiddleware(thunk, apiMiddleware))
)

export default (
  <Provider store={store}>
    <Router>
      <Switch>
        <Route exact path='/' component={MainViewContainer} />
        <Route exact path='/:id' component={DetailsViewContainer} />
      </Switch>
    </Router>
  </Provider>
)
